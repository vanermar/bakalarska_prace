IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210201094019_CreateDb', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Roles] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [Schools] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    CONSTRAINT [PK_Schools] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [Users] (
    [Id] int NOT NULL IDENTITY,
    [FirstName] nvarchar(max) NOT NULL,
    [LastName] nvarchar(max) NOT NULL,
    [Email] nvarchar(max) NOT NULL,
    [Password] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [UserRole] (
    [UserId] int NOT NULL,
    [RoleId] int NOT NULL,
    CONSTRAINT [PK_UserRole] PRIMARY KEY ([UserId], [RoleId]),
    CONSTRAINT [FK_UserRole_Roles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [Roles] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_UserRole_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_UserRole_RoleId] ON [UserRole] ([RoleId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210201094252_AddUsersAndRoles', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Users]') AND [c].[name] = N'Password');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Users] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Users] ALTER COLUMN [Password] nvarchar(250) NOT NULL;
GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Users]') AND [c].[name] = N'LastName');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [Users] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [Users] ALTER COLUMN [LastName] nvarchar(100) NOT NULL;
GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Users]') AND [c].[name] = N'FirstName');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [Users] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [Users] ALTER COLUMN [FirstName] nvarchar(100) NOT NULL;
GO

DECLARE @var3 sysname;
SELECT @var3 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Users]') AND [c].[name] = N'Email');
IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [Users] DROP CONSTRAINT [' + @var3 + '];');
ALTER TABLE [Users] ALTER COLUMN [Email] nvarchar(200) NOT NULL;
GO

DECLARE @var4 sysname;
SELECT @var4 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Roles]') AND [c].[name] = N'Name');
IF @var4 IS NOT NULL EXEC(N'ALTER TABLE [Roles] DROP CONSTRAINT [' + @var4 + '];');
ALTER TABLE [Roles] ALTER COLUMN [Name] nvarchar(150) NOT NULL;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210201103533_UserAndRoleStringLength', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [Users] ADD [ResetToken] nvarchar(250) NULL;
GO

ALTER TABLE [Users] ADD [ResetTokenTimestamp] datetime2 NULL;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210203190246_ResetPasswordRows', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [Users] ADD [Address] nvarchar(250) NOT NULL DEFAULT N'';
GO

CREATE TABLE [Parent] (
    [Id] int NOT NULL,
    [PhoneNumber] nvarchar(100) NOT NULL,
    CONSTRAINT [PK_Parent] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Parent_Users_Id] FOREIGN KEY ([Id]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [Subjects] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(200) NOT NULL,
    CONSTRAINT [PK_Subjects] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [Teacher] (
    [Id] int NOT NULL,
    [HireDate] datetime2 NOT NULL,
    CONSTRAINT [PK_Teacher] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Teacher_Users_Id] FOREIGN KEY ([Id]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [ClassTeacher] (
    [Id] int NOT NULL,
    CONSTRAINT [PK_ClassTeacher] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ClassTeacher_Teacher_Id] FOREIGN KEY ([Id]) REFERENCES [Teacher] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [TeacherSubject] (
    [TeacherId] int NOT NULL,
    [SubjectId] int NOT NULL,
    CONSTRAINT [PK_TeacherSubject] PRIMARY KEY ([SubjectId], [TeacherId]),
    CONSTRAINT [FK_TeacherSubject_Subjects_SubjectId] FOREIGN KEY ([SubjectId]) REFERENCES [Subjects] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_TeacherSubject_Teacher_TeacherId] FOREIGN KEY ([TeacherId]) REFERENCES [Teacher] ([Id]) ON DELETE CASCADE
);
GO

CREATE TABLE [Classes] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(200) NOT NULL,
    [ClassId] int NOT NULL,
    CONSTRAINT [PK_Classes] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Classes_ClassTeacher_ClassId] FOREIGN KEY ([ClassId]) REFERENCES [ClassTeacher] ([Id]) ON DELETE CASCADE
);
GO

CREATE TABLE [Student] (
    [Id] int NOT NULL,
    [EnrollmentDate] datetime2 NOT NULL,
    [CompletionDate] datetime2 NULL,
    [ClassId] int NULL,
    CONSTRAINT [PK_Student] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Student_Classes_ClassId] FOREIGN KEY ([ClassId]) REFERENCES [Classes] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Student_Users_Id] FOREIGN KEY ([Id]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [ParentStudent] (
    [ChildrensId] int NOT NULL,
    [ParentsId] int NOT NULL,
    CONSTRAINT [PK_ParentStudent] PRIMARY KEY ([ChildrensId], [ParentsId]),
    CONSTRAINT [FK_ParentStudent_Parent_ParentsId] FOREIGN KEY ([ParentsId]) REFERENCES [Parent] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_ParentStudent_Student_ChildrensId] FOREIGN KEY ([ChildrensId]) REFERENCES [Student] ([Id]) ON DELETE CASCADE
);
GO

CREATE UNIQUE INDEX [IX_Users_Email] ON [Users] ([Email]);
GO

CREATE UNIQUE INDEX [IX_Classes_ClassId] ON [Classes] ([ClassId]);
GO

CREATE INDEX [IX_ParentStudent_ParentsId] ON [ParentStudent] ([ParentsId]);
GO

CREATE INDEX [IX_Student_ClassId] ON [Student] ([ClassId]);
GO

CREATE INDEX [IX_TeacherSubject_TeacherId] ON [TeacherSubject] ([TeacherId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210206111156_AddingSubjectClassAndUsers', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [ParentStudent] DROP CONSTRAINT [FK_ParentStudent_Parent_ParentsId];
GO

ALTER TABLE [ParentStudent] DROP CONSTRAINT [FK_ParentStudent_Student_ChildrensId];
GO

EXEC sp_rename N'[ParentStudent].[ParentsId]', N'StudentId', N'COLUMN';
GO

EXEC sp_rename N'[ParentStudent].[ChildrensId]', N'ParentId', N'COLUMN';
GO

EXEC sp_rename N'[ParentStudent].[IX_ParentStudent_ParentsId]', N'IX_ParentStudent_StudentId', N'INDEX';
GO

ALTER TABLE [ParentStudent] ADD CONSTRAINT [FK_ParentStudent_Parent_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [Parent] ([Id]) ON DELETE CASCADE;
GO

ALTER TABLE [ParentStudent] ADD CONSTRAINT [FK_ParentStudent_Student_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [Student] ([Id]) ON DELETE CASCADE;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210206170227_AddingParentStudentTable', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [Classes] DROP CONSTRAINT [FK_Classes_ClassTeacher_ClassId];
GO

DROP INDEX [IX_Classes_ClassId] ON [Classes];
GO

DECLARE @var5 sysname;
SELECT @var5 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Classes]') AND [c].[name] = N'ClassId');
IF @var5 IS NOT NULL EXEC(N'ALTER TABLE [Classes] DROP CONSTRAINT [' + @var5 + '];');
ALTER TABLE [Classes] DROP COLUMN [ClassId];
GO

ALTER TABLE [ClassTeacher] ADD [ClassId] int NOT NULL DEFAULT 0;
GO

CREATE UNIQUE INDEX [IX_ClassTeacher_ClassId] ON [ClassTeacher] ([ClassId]) WHERE [ClassId] IS NOT NULL;
GO

ALTER TABLE [ClassTeacher] ADD CONSTRAINT [FK_ClassTeacher_Classes_ClassId] FOREIGN KEY ([ClassId]) REFERENCES [Classes] ([Id]) ON DELETE CASCADE;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210207135000_FixClassClassTeacherRelationship', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [Subjects] ADD [Code] nvarchar(100) NOT NULL DEFAULT N'';
GO

CREATE UNIQUE INDEX [IX_Subjects_Code] ON [Subjects] ([Code]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210209135255_AddedCodeToSUbjectTable', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Classbooks] (
    [Id] int NOT NULL IDENTITY,
    [ClassId] int NULL,
    [SchoolYear] nvarchar(200) NOT NULL,
    [IsActive] bit NOT NULL,
    CONSTRAINT [PK_Classbooks] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Classbooks_Classes_ClassId] FOREIGN KEY ([ClassId]) REFERENCES [Classes] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [Records] (
    [Id] int NOT NULL IDENTITY,
    [Created] datetime2 NOT NULL,
    [SerialNumber] int NOT NULL,
    [SubjectId] int NOT NULL,
    [Topic] nvarchar(500) NOT NULL,
    [CreatedById] int NOT NULL,
    [ClassbookId] int NOT NULL,
    CONSTRAINT [PK_Records] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Records_Classbooks_ClassbookId] FOREIGN KEY ([ClassbookId]) REFERENCES [Classbooks] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Records_Subjects_SubjectId] FOREIGN KEY ([SubjectId]) REFERENCES [Subjects] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Records_Teacher_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Teacher] ([Id]) ON DELETE CASCADE
);
GO

CREATE TABLE [Attendances] (
    [Id] int NOT NULL IDENTITY,
    [RecordId] int NOT NULL,
    [StudentId] int NOT NULL,
    [Present] bit NOT NULL,
    [IsExcused] bit NOT NULL,
    [LateArrival] bit NOT NULL,
    [AbsenceInterval] int NOT NULL,
    CONSTRAINT [PK_Attendances] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Attendances_Records_RecordId] FOREIGN KEY ([RecordId]) REFERENCES [Records] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Attendances_Student_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [Student] ([Id]) ON DELETE CASCADE
);
GO

CREATE TABLE [Events] (
    [Id] int NOT NULL IDENTITY,
    [RecordId] int NOT NULL,
    CONSTRAINT [PK_Events] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Events_Records_RecordId] FOREIGN KEY ([RecordId]) REFERENCES [Records] ([Id]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_Attendances_RecordId] ON [Attendances] ([RecordId]);
GO

CREATE INDEX [IX_Attendances_StudentId] ON [Attendances] ([StudentId]);
GO

CREATE INDEX [IX_Classbooks_ClassId] ON [Classbooks] ([ClassId]);
GO

CREATE INDEX [IX_Events_RecordId] ON [Events] ([RecordId]);
GO

CREATE INDEX [IX_Records_ClassbookId] ON [Records] ([ClassbookId]);
GO

CREATE INDEX [IX_Records_CreatedById] ON [Records] ([CreatedById]);
GO

CREATE INDEX [IX_Records_SubjectId] ON [Records] ([SubjectId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210211090212_AddedClassbookEntities', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [Classbooks] DROP CONSTRAINT [FK_Classbooks_Classes_ClassId];
GO

ALTER TABLE [Records] DROP CONSTRAINT [FK_Records_Teacher_CreatedById];
GO

EXEC sp_rename N'[Records].[CreatedById]', N'TeacherId', N'COLUMN';
GO

EXEC sp_rename N'[Records].[IX_Records_CreatedById]', N'IX_Records_TeacherId', N'INDEX';
GO

DROP INDEX [IX_Classbooks_ClassId] ON [Classbooks];
DECLARE @var6 sysname;
SELECT @var6 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Classbooks]') AND [c].[name] = N'ClassId');
IF @var6 IS NOT NULL EXEC(N'ALTER TABLE [Classbooks] DROP CONSTRAINT [' + @var6 + '];');
ALTER TABLE [Classbooks] ALTER COLUMN [ClassId] int NOT NULL;
ALTER TABLE [Classbooks] ADD DEFAULT 0 FOR [ClassId];
CREATE INDEX [IX_Classbooks_ClassId] ON [Classbooks] ([ClassId]);
GO

ALTER TABLE [Classbooks] ADD CONSTRAINT [FK_Classbooks_Classes_ClassId] FOREIGN KEY ([ClassId]) REFERENCES [Classes] ([Id]) ON DELETE CASCADE;
GO

ALTER TABLE [Records] ADD CONSTRAINT [FK_Records_Teacher_TeacherId] FOREIGN KEY ([TeacherId]) REFERENCES [Teacher] ([Id]) ON DELETE CASCADE;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210212140133_AddedForeignKeys', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [Records] ADD [IsSubstituted] bit NOT NULL DEFAULT CAST(0 AS bit);
GO

CREATE TABLE [Homework] (
    [Id] int NOT NULL,
    [Title] nvarchar(250) NOT NULL,
    [Text] nvarchar(max) NOT NULL,
    [Deadline] datetime2 NOT NULL,
    CONSTRAINT [PK_Homework] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Homework_Events_Id] FOREIGN KEY ([Id]) REFERENCES [Events] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [Inspection] (
    [Id] int NOT NULL,
    [Text] nvarchar(max) NOT NULL,
    [Auditor] nvarchar(250) NOT NULL,
    CONSTRAINT [PK_Inspection] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Inspection_Events_Id] FOREIGN KEY ([Id]) REFERENCES [Events] ([Id]) ON DELETE NO ACTION
);
GO

CREATE TABLE [Instruction] (
    [Id] int NOT NULL,
    [AuthorId] int NULL,
    [Title] nvarchar(250) NOT NULL,
    [Text] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Instruction] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Instruction_Events_Id] FOREIGN KEY ([Id]) REFERENCES [Events] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Instruction_Users_AuthorId] FOREIGN KEY ([AuthorId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);
GO

CREATE INDEX [IX_Instruction_AuthorId] ON [Instruction] ([AuthorId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210214115234_AddEvents', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [SchoolHomeNotes] (
    [Id] int NOT NULL IDENTITY,
    [Created] datetime2 NOT NULL,
    [StudentId] int NOT NULL,
    [TeacherId] int NOT NULL,
    [Note] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_SchoolHomeNotes] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_SchoolHomeNotes_Student_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [Student] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_SchoolHomeNotes_Teacher_TeacherId] FOREIGN KEY ([TeacherId]) REFERENCES [Teacher] ([Id]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_SchoolHomeNotes_StudentId] ON [SchoolHomeNotes] ([StudentId]);
GO

CREATE INDEX [IX_SchoolHomeNotes_TeacherId] ON [SchoolHomeNotes] ([TeacherId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210215175130_AddSchoolHomeNote', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [SchoolHomeNotes] ADD [Show] bit NOT NULL DEFAULT CAST(1 AS bit);
GO

ALTER TABLE [Homework] ADD [Show] bit NOT NULL DEFAULT CAST(1 AS bit);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210218102303_AddShowLabelToHomeworkAndSchoolHomeNote', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [Attendances] ADD [AbsentNoteId] int NULL;
GO

CREATE TABLE [AbsentNoteStates] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(150) NOT NULL,
    CONSTRAINT [PK_AbsentNoteStates] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [AbsentNotes] (
    [id] int NOT NULL IDENTITY,
    [Created] datetime2 NOT NULL,
    [ParentId] int NOT NULL,
    [StateId] int NOT NULL,
    CONSTRAINT [PK_AbsentNotes] PRIMARY KEY ([id]),
    CONSTRAINT [FK_AbsentNotes_AbsentNoteStates_StateId] FOREIGN KEY ([StateId]) REFERENCES [AbsentNoteStates] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_AbsentNotes_Parent_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [Parent] ([Id]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_Attendances_AbsentNoteId] ON [Attendances] ([AbsentNoteId]);
GO

CREATE INDEX [IX_AbsentNotes_ParentId] ON [AbsentNotes] ([ParentId]);
GO

CREATE INDEX [IX_AbsentNotes_StateId] ON [AbsentNotes] ([StateId]);
GO

ALTER TABLE [Attendances] ADD CONSTRAINT [FK_Attendances_AbsentNotes_AbsentNoteId] FOREIGN KEY ([AbsentNoteId]) REFERENCES [AbsentNotes] ([id]) ON DELETE NO ACTION;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210219095739_AddAbsentNote', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

EXEC sp_rename N'[AbsentNotes].[id]', N'Id', N'COLUMN';
GO

ALTER TABLE [AbsentNotes] ADD [Text] nvarchar(max) NOT NULL DEFAULT N'';
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210219155339_AddAbsentNoteText', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [AbsentNotes] ADD [StudentId] int NOT NULL DEFAULT 0;
GO

CREATE INDEX [IX_AbsentNotes_StudentId] ON [AbsentNotes] ([StudentId]);
GO

ALTER TABLE [AbsentNotes] ADD CONSTRAINT [FK_AbsentNotes_Student_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [Student] ([Id]) ON DELETE CASCADE;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210220131109_AddStudentAbsentNotes', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [AbsentNotes] DROP CONSTRAINT [FK_AbsentNotes_Parent_ParentId];
GO

DECLARE @var7 sysname;
SELECT @var7 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[AbsentNotes]') AND [c].[name] = N'ParentId');
IF @var7 IS NOT NULL EXEC(N'ALTER TABLE [AbsentNotes] DROP CONSTRAINT [' + @var7 + '];');
ALTER TABLE [AbsentNotes] ALTER COLUMN [ParentId] int NULL;
GO

ALTER TABLE [AbsentNotes] ADD [ClassTeacherId] int NULL;
GO

CREATE INDEX [IX_AbsentNotes_ClassTeacherId] ON [AbsentNotes] ([ClassTeacherId]);
GO

ALTER TABLE [AbsentNotes] ADD CONSTRAINT [FK_AbsentNotes_ClassTeacher_ClassTeacherId] FOREIGN KEY ([ClassTeacherId]) REFERENCES [ClassTeacher] ([Id]) ON DELETE NO ACTION;
GO

ALTER TABLE [AbsentNotes] ADD CONSTRAINT [FK_AbsentNotes_Parent_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [Parent] ([Id]) ON DELETE NO ACTION;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210221111902_AddAbsentNotesToClassTeacher', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [ClassSubject] (
    [ClassId] int NOT NULL,
    [SubjectId] int NOT NULL,
    CONSTRAINT [PK_ClassSubject] PRIMARY KEY ([ClassId], [SubjectId]),
    CONSTRAINT [FK_ClassSubject_Classes_ClassId] FOREIGN KEY ([ClassId]) REFERENCES [Classes] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_ClassSubject_Subjects_SubjectId] FOREIGN KEY ([SubjectId]) REFERENCES [Subjects] ([Id]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_ClassSubject_SubjectId] ON [ClassSubject] ([SubjectId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210225091029_AddSubjectClassRelation', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

EXEC sp_rename N'[SchoolHomeNotes].[Show]', N'ShowStudent', N'COLUMN';
GO

EXEC sp_rename N'[Homework].[Show]', N'ShowStudent', N'COLUMN';
GO

ALTER TABLE [SchoolHomeNotes] ADD [ShowParent] bit NOT NULL DEFAULT CAST(1 AS bit);
GO

ALTER TABLE [Homework] ADD [ShowParent] bit NOT NULL DEFAULT CAST(1 AS bit);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210225142316_AddShowToStudent', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [Users] ADD [BirthLocation] nvarchar(max) NULL;
GO

ALTER TABLE [Users] ADD [Citizenship] nvarchar(max) NULL;
GO

ALTER TABLE [Users] ADD [DateOfBirth] datetime2 NULL;
GO

ALTER TABLE [Users] ADD [Gender] bit NULL;
GO

ALTER TABLE [Users] ADD [PersonalIdentificationNumber] nvarchar(max) NULL;
GO

ALTER TABLE [Users] ADD [TitleAfter] nvarchar(max) NULL;
GO

ALTER TABLE [Users] ADD [TitleBefore] nvarchar(max) NULL;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210301130116_AddUserInfo', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

DECLARE @var8 sysname;
SELECT @var8 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Schools]') AND [c].[name] = N'Name');
IF @var8 IS NOT NULL EXEC(N'ALTER TABLE [Schools] DROP CONSTRAINT [' + @var8 + '];');
ALTER TABLE [Schools] ALTER COLUMN [Name] nvarchar(max) NOT NULL;
ALTER TABLE [Schools] ADD DEFAULT N'' FOR [Name];
GO

ALTER TABLE [Schools] ADD [Address] nvarchar(max) NOT NULL DEFAULT N'';
GO

ALTER TABLE [Schools] ADD [CIN] nvarchar(250) NOT NULL DEFAULT N'';
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210312093144_AddInfoAboutSchool', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [ClassTeacher] DROP CONSTRAINT [FK_ClassTeacher_Classes_ClassId];
GO

DROP INDEX [IX_ClassTeacher_ClassId] ON [ClassTeacher];
GO

DECLARE @var9 sysname;
SELECT @var9 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ClassTeacher]') AND [c].[name] = N'ClassId');
IF @var9 IS NOT NULL EXEC(N'ALTER TABLE [ClassTeacher] DROP CONSTRAINT [' + @var9 + '];');
ALTER TABLE [ClassTeacher] ALTER COLUMN [ClassId] int NULL;
GO

CREATE UNIQUE INDEX [IX_ClassTeacher_ClassId] ON [ClassTeacher] ([ClassId]);
GO

ALTER TABLE [ClassTeacher] ADD CONSTRAINT [FK_ClassTeacher_Classes_ClassId] FOREIGN KEY ([ClassId]) REFERENCES [Classes] ([Id]) ON DELETE NO ACTION;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210321181056_ClassTeacherNullableClassId', N'5.0.2');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

DROP INDEX [IX_ClassTeacher_ClassId] ON [ClassTeacher];
GO

CREATE INDEX [IX_ClassTeacher_ClassId] ON [ClassTeacher] ([ClassId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210322150809_ClassTeacherDropIndex', N'5.0.2');
GO

COMMIT;
GO

INSERT INTO [dbo].[AbsentNoteStates]
           ([Name])
     VALUES
           ('Odeslána'),
           ('Zamítnuta'),
	   ('Potvrzena')
GO

INSERT INTO [dbo].[Roles]
           ([Name])
     VALUES
           ('Admin'),
           ('Učitel'),
	   ('Třídní učitel'),
	   ('Ředitel'),
	   ('Zástupce ředitele'),
	   ('Rodič'),
	   ('Žák')
GO

INSERT INTO [dbo].[Users]
           ([FirstName]
           ,[LastName]
           ,[Email]
           ,[Password]
           ,[ResetToken]
           ,[ResetTokenTimestamp]
           ,[Address]
           ,[BirthLocation]
           ,[Citizenship]
           ,[DateOfBirth]
           ,[Gender]
           ,[PersonalIdentificationNumber]
           ,[TitleAfter]
           ,[TitleBefore])
     VALUES
           ('Admin'
           ,'Admin'
           ,'admin@admin.cz'
           ,'$2b$10$xEWe772ijx7jHamhznY7J.75ITr2jC7kbhGR02MMxUCJgEQfxdW3i'
           ,NULL
           ,NULL
           ,'Adresa'
           ,'Location'
           ,'CZ'
           ,'1986-05-28 00:00:00.0000000'
           ,1
           ,'1111111111'
           ,NULL
           ,NULL)
GO

INSERT INTO [dbo].[UserRole]
           ([UserId]
           ,[RoleId])
     VALUES
           (SCOPE_IDENTITY()
           ,(SELECT Id FROM [dbo].[Roles] WHERE Name = 'Admin'))
GO


