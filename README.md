Tento soubor slouží jako stručný popis obsahu SD karty.

readme.md - tento soubor.

publish - jedná se o adresář s verzemi aplikace, které lze nasadit na server.
	  Složka obsahuje soubor deployment.pdf, který slouží jako instalační příručka
	  a dále popisuje obsah složek MSSQL a PostgreSQL, které jsou součástí složky publish.

ElectronicClassbook - složka, která obsahuje zdrojové kódy k praktické části práce.

thesis - obsahuje soubory související s textem práce. Obsahuje složku src,
	 kde se nachází zdrojová forma práce ve formátu LATEX,
	 soubor BP_Vaner_Martin_2021, což je bakalářská práce ve formátu PDF,
	 soubor assignment.pdf, což je soubor obsahující zadání bakalářské práce.

samples - složka s ukázkami aplikace. Adresář obsahuje složku screenshots, kde se nacházejí
	  screenshoty pořízené při práci s aplikací. Dále obsahuje soubor video.mp4, který slouží
	  jako video ukázka aplikace. Video ukázka je komentovaná.

